import setuptools
from numpy.distutils.core import Extension, setup
import subprocess
import os, sys

if sys.version_info < (2,7):
    sys.exit('Sorry, Python < 2.7 is not supported')

#################################

if ('FFTW_DIR' in os.environ):
    fftw_CFLAGS = []
    fftw_LIBS = ['-L'+os.environ['FFTW_DIR'], '-L'+os.path.join(os.environ['FFTW_DIR'],'lib'), '-lfftw3', '-lm']

else:

    #Use pkg-config (if installed) to figure out the C flags and linker flags
    #needed to compile a C program with FFTW3.
    if (sys.version_info.major == 3):
        status, output = subprocess.getstatusoutput('pkg-config fftw --exists')
    elif (sys.version_info.major == 2):
        import commands
        status, output = commands.getstatusoutput('pkg-config fftw --exists')

    if status == 0:
        configCommand = 'pkg-config fftw3'
        fftw_CFLAGS = os.popen('%s --cflags' % configCommand, 'r').readline().rstrip().split()
        fftw_LIBS = os.popen('%s --libs' % configCommand, 'r').readline().rstrip().split()
    else:
        print ("WARNING:  FFTW3 cannot be found, using defaults")
        fftw_CFLAGS = []
        fftw_LIBS = ['-lfftw3', '-lm']

##################################

PATH = './fortran/'
SOURCE_FILES = ['onedspline.f90', 'nrint.f90', 'powspec.f90', 'sort_grid_points.f90', 'pc.f90', 'es.f90', 'rsd.f90', 'fcoll_grid.f90', 'matter_fields.f90', 'matter_fields_haloes.f90', 'ionization_map.f90']

SOURCE_FILES = [PATH + files_in_list for files_in_list in SOURCE_FILES]

extra_compile_args = fftw_CFLAGS
extra_f90_compile_args = ['-ffree-form', '-ffree-line-length-none'] 
extra_link_args = fftw_LIBS

compiler_options = ['f2py=f2p73.8']

setup(name='script',
       ext_modules=[Extension(name='script_fortran_modules', sources=SOURCE_FILES, extra_compile_args=extra_compile_args, extra_f90_compile_args=extra_f90_compile_args, extra_link_args=extra_link_args)],
      packages=['script']
       )
